################################################
##USA OPEN GEODEMOGRAPHICS               #######
##CLUSTER VISUALIZATION                  #######
##MAY 30 2013                            #######
##Seth E. Spielman and Alex Singleton    #######
################################################

########################################################################
##VISUAL SUMMARY OF CLUSTER ATTRIBUTES
########################################################################
library(ggplot2)
library(reshape2)
library(plyr)
library(xtable)


##############################
##CREATE SUBSETS AROUND THEMES
##############################
plot_groups <- vars[vars$new_set==0 & is.na(vars$new_set) == FALSE, c("desc", "cat")]
cluster.cols <- c( "X2", "X10", "X31", "X55", "cluster")
##REFINE VARIABLE GROUPS FOR PLOTS
categories <- list(
  EDUCATION = plot_groups[plot_groups$cat %in% c("EDUCATION"), "desc"],
  EMPLOYMENT = plot_groups[plot_groups$cat %in% c("EMPLOYMENT"), "desc"],
  LANGUAGE = plot_groups[plot_groups$cat %in% c("LANGUAGE", "NATIVITY", "RACE"), "desc"],
  WEALTH = plot_groups[plot_groups$cat %in% c("WEALTH"), "desc"],
  HOUSING =  plot_groups[plot_groups$cat %in% c("HOUSING-WEALTH", "HOUSING"), "desc"],
  HOUSING_COST =  plot_groups[plot_groups$cat %in% c("HOUSING-WEALTH"), "desc"],
  MOBILITY = plot_groups[plot_groups$cat %in% c("MOBILITY"), "desc"],
  FAMILY_STABILITY = plot_groups[plot_groups$cat %in% c("FAMILY-STRUCT", "STABILITY"), "desc"])

#NAMES FOR THE 10 CLASS SOLUTION
grp_names <- c("A: Hispanic and Kids",
               "B: Wealthy Nuclear Families",
               "C: Middle Income, Single Family Homes",
               "D: Native American",
               "E: Wealthy Urbanites",
               "F: Low Income and Diverse",
               "G: Old, Wealthy White",
               "H: Low Income Minority Mix",
               "I: Poor, African-American",
               "J: Residential Institutions, Young People")

#################################
##CLUSTER SUMMARIES
#################################
##STANDARDIZE TO FACILITATE DISPLAY
cluster.cols <- c("X10")
for (grp in categories){
  for (cl in cluster.cols){
    if (sum(grp %in% names(usa.trt.cl)) != length(grp)) {
      grp <- grp[grp %in% names(usa.trt.cl)]}
    dataM <- melt(
      data.frame(scale(usa.trt.cl[complete.cases(usa.trt.cl),as.character(grp)]), 
                 cat=usa.trt.cl[complete.cases(usa.trt.cl), as.character(cl)]), id="cat")
    plot1 <- ggplot(dataM, aes(group=variable, x=variable, y=value, fill=variable)) + 
      stat_summary(fun.y = "mean", geom="bar") + facet_grid(.~ cat, scales="free")  + opts(
        axis.text.x = theme_text(size=6, angle=45, hjust=1, vjust=1),
        title="Open Geodemographic \nCommunity Types") + 
      ylab("Variable Mean (Standard Score)") + 
      xlab("Variables by Community Type") 
    print(plot1)
  }}


#####################################
##HEATMAPS OF INDEX SCORES BY DOMAIN
#####################################
##LOAD DATA
load("tract_data_with_classes_063013.Rdata")
load("gini_master.Rdata")
variables <- read.csv("usa_trt_varnames_plots_0913.csv")

##CALCULATE INDEX SCORES
usa.trt.cl$X10 <- factor(x=usa.trt.cl$X10, levels=1:10, labels=LETTERS[seq( from = 1, to = 10 )])
usa.trt.indexScores <- 
  as.data.frame(apply(usa.trt.cl[,5:140], MARGIN=2, FUN=function(x)  (x/mean(x, na.rm=TRUE)) * 100))
usa.trt.indexScores <- cbind(usa.trt.cl[,c(2:4, 141:144)], usa.trt.indexScores)

#tab_summary <- aggregate(.~ X10, data=usa.trt.indexScores[,c(4,8:143)], FUN=summary)
tab_mean  <- aggregate(.~ X10, data=usa.trt.indexScores[,c(4,8:143)], FUN=mean)



##HEATMAPS BY DOMAIN
for (d in levels(variables$Domain)[2:11]){
  vars <- variables[variables$new_set==0 & variables$Domain==d, c("desc", "plotlab")]
  print(paste("DomainSummary", d, ".pdf" ,sep=""))
  assign(d, usa.trt.indexScores[,c("X10", as.character(vars$desc))])
  #eval(substitute(names(x)<-c("X10", as.character(vars$plotlab)), list(x=as.symbol(d))))
  assign(d, aggregate(.~ X10, data=get(d), FUN=mean))
  assign(d, data.frame(X10=get(d)[,1], round(get(d)[,2:dim(get(d))[2]])))
  assign(paste(d, ".m", sep=""), melt(get(d), id="X10"))
  p <- ggplot(get(paste(d, ".m", sep="")), aes(variable, X10, label=value)) +  
    geom_tile(aes(fill = value)) + geom_text(size=3) +
    scale_fill_gradientn(colours=c("#D7191C","#D7191C", "#FDAE61", "#F5F5F5", "#A6D96A", "#1A9641", "#1A9641"),
                         values=c(0, 20, 60, 90, 110, 150, 200, max(get(paste(d, ".m", sep=""))$value)), 
                         breaks=c(0, 50, 100, 150, 200, max(get(paste(d, ".m", sep=""))$value)), 
                         rescaler = function(x, ...) x, oob = identity, name="Index\nScore") +
    scale_y_discrete(expand=c(0,0)) +
    scale_x_discrete(expand=c(0,0), labels=as.character(vars$plotlab)) +
    theme(axis.text.x = element_text(angle = 90, hjust = .5, size=6),
          axis.text.y = element_text(size=6),
          axis.title = element_blank()) + 
    #ylab("Class") + xlab("Measure") +
    #ggtitle(paste(d, "Measures by Class")) + 
    theme(legend.position = "none")
  #ggsave(p, filename=paste("DomainSummary", gsub(pattern=" ", replacement="_", x=d), ".pdf" ,sep=""), height=5, width=(dim(get(d))[2]/2))
}
rm(Demography, Education, "Family Structure", Housing, "Industry of Occupation", Language, Mobility, Race, Stability, Wealth,
   Demography.m, Education.m, "Family Structure.m", Housing.m, "Industry of Occupation.m", Language.m, Mobility.m, Race.m, Stability.m, Wealth.m, vars)

#print a html table of means by class
#print(xtable(tab_mean), type="html", file="~/table.html")

##############################
##POPULATION PYRAMIDS
##############################

##EXTRACT AGE RELATED VARIABLES
plot_vars<-levels(variables$Profile_Plots)[2:17]

age <- usa.trt.cl[,c("X10", as.character(variables[variables$Profile_Plots == "Age_Structure", "desc"]))]

##Merge age categories to make equal range
age$PCT_Male_15_To_19_Years <- age$PCT_Male_18_And_19_Years  +  age$PCT_Male_15_To_17_Years  
age$PCT_Male_20_To_24_Years <- age$PCT_Male_20_Years  + age$PCT_Male_21_Years  + age$PCT_Male_22_To_24_Years   
age$PCT_Male_60_To_64_Years <- age$PCT_Male_60_And_61_Years    +  age$PCT_Male_62_To_64_Years   
age$PCT_Male_65_To_69_Years <- age$PCT_Male_65_And_66_Years  + age$PCT_Male_67_To_69_Years 

age$PCT_Female_15_To_19_Years <- age$PCT_Female_18_And_19_Years  +  age$PCT_Female_15_To_17_Years  
age$PCT_Female_20_To_24_Years <- age$PCT_Female_20_Years  + age$PCT_Female_21_Years  + age$PCT_Female_22_To_24_Years   
age$PCT_Female_60_To_64_Years <- age$PCT_Female_60_And_61_Years    +  age$PCT_Female_62_To_64_Years   
age$PCT_Female_65_To_69_Years <- age$PCT_Female_65_And_66_Years  + age$PCT_Female_67_To_69_Years 

age <- age[,c(1,2,3,4,48,49,10:16,50,51,21:24,25,26,27,52,53,33:39,54,55,44:47)]

age_m <- melt(age, id.vars="X10")
#age_m$variable <- as.factor(age_m$variable, ordered=TRUE)

##SUMMARIZE VARIABLES BY GROUP
age_m <- aggregate(value~variable + X10, data=age_m, FUN=summary, na.rm=TRUE)

##ATTRIBUTE GENDER TO EARCH ROW FOR SUBSETTING
age_m$gender <- ifelse(
  age_m$variable %in% 
    names(age)[2:19],
  "Male", "Female")

##ADD MEAN ANF QUARTILES TO SUMMARY TABLE
age_m$Mean <- age_m$value[,4]
age_m$Q1 <- age_m$value[,2]
age_m$Q3 <- age_m$value[,5]

##REMOVE "SUMMARY" MATRIX FROM DATA FRAME
age_m <- age_m[,c(1,2,4,5,6,7)]

##STANDARDIZE VARIABLE NAMES FOR MALE AND FEMALE
##PRESERVE ORDER OF VARIABLE
age_mm <- age_m[age_m$gender=="Male", ]
age_mf <- age_m[age_m$gender=="Female", ]
age_mm$variable <- factor(age_mm$variable, ordered=TRUE) 
age_mf$variable <- factor(age_mf$variable, ordered=TRUE) 
levels(age_mm$variable) <- substr(levels(age_mm$variable), start=10, stop=nchar(as.character(levels(age_mm$variable))))
levels(age_mf$variable) <- substr(levels(age_mf$variable), start=12, stop=nchar(as.character(levels(age_mf$variable))))

##MAKE ALL FEMALE DATA NEGATIVE FOR PLOTTING
age_mf$Mean <-  age_mf$Mean * -1
age_mf$Q1 <-  age_mf$Q1 * -1
age_mf$Q3 <-  age_mf$Q3 * -1

###PLOT POPULATION PYRAMIDS
ggplot(data=age_mm,
       aes(y=Mean, x=variable, ymin=Q1, ymax=Q3)) + 
  geom_pointrange() + facet_grid(X10~.) +
  geom_pointrange(data=age_mf,
                  aes(y=Mean, x=variable, ymin=Q1, ymax=Q3)) +
  theme(axis.text.y = element_blank(),
        axis.text.x = element_blank(),
        axis.title = element_blank(),
        panel.grid.minor.y=element_blank(), panel.grid.major.y=element_blank()) +
  coord_flip()

#cleanup
rm(age,age_m, age_mm, age_mf)

########################################
##HEATMAPS FOR TYPES (SUBCLASSES OF X10)
########################################
##HEATMAPS BY DOMAIN
##FOR SUBCLASS K
#K <- 4 #GROUP NUMBER
#subclasses <-  2 #number of subclasses for class K
##FOR EXAMPLE GROUP 2 HAS 11 SUBCLASSES (TYPES)
K <- 2 
subclasses <-  11 #number of subclasses for class K
for (d in levels(variables$Domain)[2:11]){
  vars <- variables[variables$new_set==0 & variables$Domain==d, c("desc", "plotlab")]
  print(paste("DomainSummary", d, ".pdf" ,sep=""))
  assign(d, usa.trt.indexScores[usa.trt.indexScores$X10 == K,c("X55", as.character(vars$desc))])
  assign(d, 
         aggregate(.~ X55, data=get(d), FUN=mean))
  assign(paste(d, ".m", sep=""), melt(round(get(d)), id="X55"))
  p <- ggplot(get(paste(d, ".m", sep="")), aes(x=variable, y=as.character(X55), label=value)) +  
    geom_tile(aes(fill = value)) + geom_text(size=3) +
    scale_fill_gradientn(colours=c("#D7191C","#D7191C", "#FDAE61", "#F5F5F5", "#A6D96A", "#1A9641", "#1A9641"),
                         values=c(0, 20, 60, 90, 110, 150, 200, max(get(paste(d, ".m", sep=""))$value)), 
                         breaks=c(0, 50, 100, 150, 200, max(get(paste(d, ".m", sep=""))$value)), 
                         rescaler = function(x, ...) x, oob = identity, name="Index\nScore") +
    scale_y_discrete(expand=c(0,0)) +
    scale_x_discrete(expand=c(0,0), labels=as.character(vars$plotlab)) +
    theme(axis.text.x = element_text(angle = 90, hjust = .5),
          axis.title = element_blank()) + 
    #ylab("Class") + xlab("Measure") +
    #ggtitle(paste(d, "Measures by Class")) + 
    theme(legend.position = "none")
  ggsave(p, filename=paste("DomainSummary55class", K, gsub(pattern=" ", replacement="_", x=d), ".pdf" ,sep=""), height=5, width=(dim(get(d))[2]/2))
}


#########################################
##RANK VARIABLES
##NOT USED IN ANALYSIS
##CODE IS NOT COMPLETE DEVELOPED
#########################################
ranks <- matrix(nrow=250, ncol=136)
for (i in 1:250){
  ranks[i,] <- ave(final$centers[i,], FUN=rank)
}

###LOOKUP CLUSTER RANKS
cluster.rank <- function(cluster.number){
  varN <- names(usa.trt.cat72[,-c(141, 2, 3, 4)])
  return(varN[ranks[cluster.number,]])}

cluster.rank(1)[c(1:5, 131:136)]
cluster.rank(2)[c(1:5, 131:136)]
cluster.rank(37)

a <- aggregate(x=usa.trt.cat72[complete.cases(usa.trt.cat72),-c(141, 2, 3, 4)], by=usa.trt.cat72[,144], mean, simplify = TRUE)
aa <- dt[,mean(usa.trt.cat72[complete.cases(usa.trt.cat72),-c(141, 2, 3, 4)]),by=usa.trt.cat72[,144]]

head(melt(final$centers, id=rep(1:250, 136)), 208)