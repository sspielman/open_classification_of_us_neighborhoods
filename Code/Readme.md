#Using the code in this repository

In the course of this analysis we have tried to make our code as explicit as possible.  We have tried to use only mainstream R packages.  

The code is organized roughly into four parts.

1.  Data preparation
2.  Analysis
3.  Visualization
4.  Evaluation

We have included binaries (in the RData Files) folder that contain our output from critical points in each anaylsis.  